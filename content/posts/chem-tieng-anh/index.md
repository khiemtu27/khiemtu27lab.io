---
title: Chém tiếng Anh
tags:
  - thoughts
date: 2023-04-20
slug: chem-tieng-anh
---
Mình thấy bản thân còn dở khi lỡ đặt vào câu nói của mình một từ tiếng Anh trong vô thức. Trước đây mình từng nghĩ vậy là hay ho, đôi khi có từ đơn giản thôi nhưng vẫn cố dùng tiếng Anh.

Bất kể lý do là gì, gần đây chính mình không cho phép mình lạm dụng tiếng Anh không đúng chỗ như vậy. Chắc vì quá nhiều lần "đội quần" rồi nên mình cũng chả còn cái mình để thể hiện...

---

Nếu như phải sống trong môi trường không dùng tiếng Việt thường xuyên, việc bảo mình quên tiếng mẹ đẻ vì đơn giản mình không dùng nó nhiều thể hiện một điều mình đã đạt tới giới hạn trong trí nhớ và sự linh hoạt của mình.

Nếu đủ nhạy bén thì sử dụng một ngôn ngữ cũng đơn giản như việc lái xe, biết đi xe bốn bánh không có nghĩa mình sẽ quên hết cảm giác lái xe hai bánh rồi lại vụng về với chiếc xe đạp như một đứa trẻ.

Thế nên nếu mình cứ khăng khăng bao biện rằng vì môi trường dùng A nhiều hơn B buộc mình phải quên B thì e rằng bộ nhớ của mình lúc này bắt đầu đầy và có dấu hiệu ghi đè. Điều cần làm lúc này là cải thiện trí nhớ và độ nhạy bén chứ không phải cố phơi bày khuyết điểm của mình bằng cách thêm những từ tiếng Anh vào câu nói.

---

Mình có thể biết banana là một quả chuối, cùng một chủ thể mình có hai từ trong hai ngôn ngữ để nhớ, nếu mình tự hào khi dùng "banana" thay vì "chuối" để nói chuyện với người Việt thì một là mình chỉ đang phức tạp hoá câu chuyện bằng việc pha vào lời nói của mình một ngôn ngữ khác, hai là mình vẫn còn khá kém cỏi trong việc vận dụng đúng lúc một trong hai ngôn ngữ.

Cả hai lý do đều không đáng để tự hào, một bên thì quá vì thể hiện bản thân mà đánh đổi hiệu quả trong giao tiếp, bên còn lại thì đơn giản là để lộ ra khuyết điểm của bộ não này.

Tiếng mẹ đẻ về bản chất đã là đủ dùng để giao tiếp với những người xung quanh, nếu mình cần vay mượn tạm thời một từ nào đó trong ngôn ngữ khác (không nói về từ mượn như radio hay TV) mình nên thấy mình còn dở vì trong suốt thời gian sống mình vẫn chưa thành thạo cái ngôn ngữ dễ tiếp cận nhất với mình để giao tiếp hiệu quả.
