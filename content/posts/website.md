---
title: Về website này
description: đây là gì?
slug: website
---
Website này là nổ lực của mình để góp một phần tí ti vào xu hướng _"xa lìa mạng xã hội tập trung"_.
Một nơi mà những ý tưởng của mình được viết ra, biểu đạt thoải mái mà không phụ thuộc vào "ngài CEO" hay công ty nào.

Mọi con chữ, ký tự, trên (thứ bạn đang đọc) và trong (html, css, javascript) website này đều không thuộc về ai cả, kể cả mình.
Có thể nói là nó thuộc về cộng đồng.
Suy cho cùng thì toàn bộ cái gọi là _kiến thức của mình_ cũng là những thứ có sẵn ngoài kia, mình chỉ gom nhặt được từ nhân loại.
Giống như nhặt được hòn đá bên đường rồi gọi nó là của mình, thật vô lý!

Bạn có thể xem, tải về, sao chép, sử dụng mọi thứ trên website này mà không cần trả mình một đồng nào cả, nhưng nếu bạn có ghi nguồn thì mình sẽ rất coi trọng điều đó!

Bản thô của website này có thể được tìm thấy tại repo [GitHub](https://github.com/khiemtu27/khiemtu27.github.io) và [GitLab](https://gitlab.com/khiemtu27/khiemtu27.gitlab.io) của mình, cũng là nơi nó sẽ được host tại server của họ, sử dụng dịch vụ [GitHub Pages](https://khiemtu27.github.io) và [GitLab Pages](https://khiemtu27.gitlab.io).
